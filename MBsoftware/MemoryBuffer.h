﻿#pragma once
#include "CPointFloat.h"
#include "CLine.h"
#include "CRectRotated.h"
//#include "Circle.h"
#include "stdafx.h"
#include "Timer.h"


using namespace std;
//Nov memory buffer predeln za QT 
//Ta CMemoryBuffer ima že Aleševe popravke za openCV ter popravek dvojnega zrcaljenja in Martinove popravke do 23.5.

//POPRAVLJENE FUNKCIJE MARTIN:
// FindCommonPointsWhiteFastBoundingBox, HorizontalIntensity, GetFirstEdgePointsVectorRadial

//DODANE FUNKCIJE MARTIN
//GetPolarCVbuffer, ImageFromPolarCVbuffer, PasteImage, FilterMatrix1D, FilterMatrix2D, FilterMatrix1Dfast, FilterMatrix2Dfast

// CMemoryBuffer
using namespace cv;


class CMemoryBuffer 
{


public:
	CMemoryBuffer();
	CMemoryBuffer(int width, int height, int depth);
	CMemoryBuffer(QString path);
	CMemoryBuffer(const CMemoryBuffer& buffer);// Copy constructor
	virtual ~CMemoryBuffer();

public:

	Mat*				buffer;

	bool				isActive;
	int					bufferSize;
	int					depth; //0: gray, 2: RGB32
	int					width;
	int					height;
	int					shapeNum;
	double				encoder;
	double				motorValue;

	int					xPosition;
	int					yPosition;

	int					whiteThreshold;
	int					blackThreshold;

	bool				isActiveEdges;
	float**				Xedges;
	float**				Yedges;

	//common points
	bool				isActiveCommonPointsWhite;
	bool				isActiveCommonPointsBlack;
	bool				isActiveIntensity;
	bool				isActiveIntensityRed;
	bool				isActiveIntensityGreen;
	bool				isActiveIntensityBlue;

	//Dodal Martin: zastavice, ki povejo, ali je že rezerviran spomin za intenzitete, poprečene v radialni, kotni smeri oziroma vzdolž stranic zasukanega pravokotnika
	bool				isActiveIntensityRadial;
	bool				isActiveIntensityAngular;
	bool				isActiveIntensityRotated;

	int**				commonPointsBufferWhite;
	int**				commonPointsBufferBlack;

	UINT16**				commonPointsWhiteX;
	UINT16**				commonPointsWhiteY;
	UINT16**				commonPointsBlackX;
	UINT16**				commonPointsBlackY;
	float**				connectedComponentsCoorX;
	float**				connectedComponentsCoorY;

	int*				seqPixTable;
	int*				numberOfPointsWhite;
	int*				numberOfPointsBlack;
	QRect*				boundingBoxWhite;
	QRect*				boundingBoxBlack;
	CPointFloat*		centerBlack;
	CPointFloat*		centerWhite;
	int**				activePointWhite;
	int**				activePointBlack;
	int					nrShapesBlack;
	int					nrShapesWhite;
	int					maxShapesWhite;
	int					maxShapesBlack;
	int					maxPointsWhite;
	int					maxPointsBlack;
	int					numShapes;
	int					stThread;
	vector<vector<CPointFloat>> commonEdgePointsWhite;
	vector<vector<CPointFloat>> commonEdgePointsBlack; //0 - left; 1 = right; 2 = top; 3 = bottom
	int					dynamicTreshold;
	QRect				edgesXRectangle; //redtangle where edges are
	QRect				edgesYRectangle; //redtangle where edges are



	//rectangle Intensity
	QRect rect;
	std::vector< CPointFloat> detectedPoints;
	std::vector< CPointFloat> detectedPointsLightToDark;
	std::vector< CPointFloat> detectedPointsDarkToLight;
	std::vector< float > intensity;
	float *intensityRed;
	float *intensityBlue;
	float *intensityGreen;

	float* intensityRadial;
	float* intensityAngular;
	float* intensityRotated;
	CPointFloat* intensityRotatedGraph;	//array s točkami grafa, ki prikazuje intensityRotated
	int intensityRotatedLength; //toliko elementov array-a intensityRotated je trenutno izračunanih


	int size;
	QPoint *position;
	bool vertical;

	int histogram[256];
	float relativeFrequencies[256];

public:

	void			Init();
	void			Swap(CMemoryBuffer &firsBuffer, CMemoryBuffer &secondBuffer);
	CMemoryBuffer	operator =(CMemoryBuffer buffer);
	bool			operator ==(CMemoryBuffer buffer);
	bool			operator !=(CMemoryBuffer buffer);
	int				Create(int width, int height, int depth);
	int				CreateCommonPointsWhite(int maxShapes, int maxPoints);
	int				CreateCommonPointsBlack(int maxShapes, int maxPoints);
	void			CreateConnectedComponentsTable(Mat labels, Mat stats);
	int				CreateIntensity();
	int				CreateIntensityBlue();
	int				CreateIntensityRed();
	int				CreateIntensityGreen();

	//Dodal Martin: rezerviramo spomin za intenzitete, poprečene v radialni, kotni smeri ali vzdolž stranic zasukanega pravokotnika
	int				CreateIntensityRadial();
	int				CreateIntensityAngular();
	int				CreateIntensityRotated();


	int				CreateEdges();
	void			DeleteBuffer();
	void			DeleteCommonPointsWhite();
	void			DeleteCommonPointsBlack();
	void			DeleteConnectedComponentsTable();
	void			DeleteEdges();
	void			DeleteIntensity();
	void			DeleteIntensityRed();
	void			DeleteIntensityGreen();
	void			DeleteIntensityBlue();

	//Dodal Martin: osvobodimo spomin za intenzitete, popreèene v radialni, kotni smeri ali vzdolž stranic zasukanega pravokotnika
	void			DeleteIntensityRadial();
	void			DeleteIntensityAngular();
	void			DeleteIntensityRotated();

	int				Copy(CMemoryBuffer newBuffer);
	int				LoadBuffer(QString path);
	int				LoadBuffer(QString path, int depth);
	int				SaveBuffer(QString path);
	int SaveBuffer(QString path, QString name);
	int				SaveBuffer(QString path, QRect roi);

	int				GetPosition(int x, int y);
	int				GetRed(int position);
	int				GetRed(int x, int y);
	int				GetGreen(int position);
	int				GetGreen(int x, int y);
	int				GetBlue(int position);
	int				GetBlue(int x, int y);
	int				GetMinIntensity(CRectRotated rec);
	int				GetMinIntensity(QRect rec);
	int				GetMinIntensity();
	int				GetMaxIntensity(CRectRotated rec);
	int				GetMaxIntensity(QRect rec);
	int				GetMaxIntensity();
	int				GetAverageIntensity(int position); //fast
	int				GetAverageIntensity(int x, int y); //slow
	int				GetThreshold(Rect region);
	int				FilterShapes(Mat labelStats, int minArea, int maxArea);

					//Dodal Martin: 

	bool			CalcDynamicTreshold(float approxTreshold, int latticePeriod);		//izraèuna dinamièno mejo intenzitete kot srednjo vrednost med belim in èrnim (belo in èrno loèi glede na približno mejo intenzitete)

					//GetInterpolatedIntensity z interpolacijo izraèuna intenziteto pri necelih koordinatah x in y  //Zaenkrat sem sprogramiral le za èrno-belo sliko
					//Vrne float intenziteto za izraèune. Èe želimo nastavljati intenziteto piksla na interpolirano vrednost, zaokrožiti na najbližjo celo vrednost
	float			GetInterpolatedIntensity(float x, float y);


	//RotateBuffer zasuka sliko za kot fi okoli izhodišèa (piksel [0,0]), nato jo skalira z zoomFactor, nato izvede translacijo za (xOffset, yOffset)
	//Èe je interpoliraj true, zmanjšamo popaèitev z interpolacijo. Èe je false, dela hitreje, toda slika se nekoliko popaèi zaradi zaokrožitve decimalnih pikslov na cele
	/*
	UPORABA RotateBuffer:
	1. CMemoryBuffer novaSlika = staraSlika;	//s copy konstruktorjem skopiramo staro sliko v novo
	2. novaSlika.RotateBuffer(staraSlika,fi,zoomFactor, xOffset,yOffset);	//buffer nove slike nastavimo na transformirano staro sliko
	3. slika = novaSlika;	//èe želimo, da je na zaslonu prikazana nova slika, skopiramo novo sliko nazaj v staro
	POZOR: Xedges, WhiteShapes ipd. je treba izraèunati še enkrat
	*/
	//bool			RotateBuffer(CMemoryBuffer* pStaraSlika, float fiDeg, float zoomFactor, float xOffset, float yOffset, bool interpoliraj);



	int				GetWhiteIntensityCount(CRectRotated polygon, int threshold);
	int				GetBlackIntensityCount(CRectRotated polygon, int threshold);
	int				GetAverageIntensity(QRect rec);
	int				GetAverageIntensity(QRect rec, int whiteThreshold, int blackThreshold);
	int				GetAverageIntensity();
	int				GetImageThresholds(CRectRotated whiteArea, CRectRotated blackArea);
	int				GetImageThresholds(QRect whiteArea, QRect blackArea);
	int				GetImageThresholds(CRectRotated area);
	int				GetImageThresholds(QRect area);
	int				GetImageThresholds();
	RGBQUAD			GetAverageIntensityRGB(QRect rec);
	void			SetAverageIntensity(int position, int value);
	void			SetAverageIntensity(int x, int y, int value); //slow
	void			SetRed(int position, int value);
	void			SetRed(int x, int y, int value);
	void			SetGreen(int position, int value);
	void			SetGreen(int x, int y, int value);
	void			SetBlue(int position, int value);
	void			SetBlue(int x, int y, int value);




	void			ClearBuffer();
	void			AddBufferVertical(CMemoryBuffer &picture);
	void			AddBufferVertical(BYTE* pBuf, int width, int height, int depth);
	void			AddBufferVertical(BYTE* pBuf, int width, int height, int depth, int yPosition);
	void			AddBufferHorizontal(CMemoryBuffer &picture);
	void			AddBufferHorizontal(BYTE* pBuf, int width, int height, int depth);
	void			AddBufferHorizontal(BYTE* pBuf, int width, int height, int depth, int xPosition);
	int				GetVerticalFocusValue();
	int				GetHorizontalFocusValue();
	void			ClearEdgesXY();
	int				FindEdges(QRect R, int threshold); //x in y robovi
	int				FindEdgesX(QRect R, int threshold);
	int				FindEdgesY(QRect R, int threshold);
	void			ClearEdgesXY(QRect R);
	void			ClearEdgesX(void);
	void			ClearEdgesY(void);

	void			Flip90andConcatenate(BYTE* pBuf, int width, int height, int depth, QRect focusRectangle, int imageIndex);
	void			Flip90andConcatenateLeft(BYTE* pBuf, int width, int height, int depth, QRect focusRectangle, int imageIndex);
	CPointFloat GetTopEdgePoint(QRect area); //top point in area
	CPointFloat GetBottomEdgePoint(QRect area);
	CPointFloat GetRightEdgePoint(QRect area);
	CPointFloat GetLeftEdgePoint(QRect area);

	CPointFloat GetTopEdgePoint(CRectRotated polygon); //top point in polygon
	CPointFloat GetBottomEdgePoint(CRectRotated polygon); //bottom point in polygon
	CPointFloat GetRightEdgePoint(CRectRotated polygon); //right point in polygon
	CPointFloat GetLeftEdgePoint(CRectRotated polygon); //left point in polygon

	CPointFloat GetCenterOfGravity(QRect area); //Opomba Martin: ta funkcija raèuna težišèe robov (prehodov intenzitete): za težišèe belega/èrnega je treba napisati drugo funkcijo
	CPointFloat GetCenterOfGravity(CRectRotated polygon);  //tezisce v poligonu

	CLine GetLine(std::vector<CRectRotated> polygons); //premica iz poligonov
	CLine GetLine(CRectRotated polygon);
	CPointFloat GetEdgePointOnSegment(CLine segment, int directionForward, int threshold); //detect first edge on segment - ce je nastgavljen threshold izvede se funkcije za iskanje robov
	CPointFloat GetEdgePointOnSegment(CLine segment, int directionForward);	//

	//Dodal Martin: prehod čez intensityTreshold z 2D interpolacijo: deluje na subpikselskem nivoju tudi na linijah, ki niso približno vzporedne x ali y
	//Če je blackToWhite true, išče le prehode iz črnega na belo, sicer 
	//išče v smeri P1->P2, če je forward = true, sicer v smeri P2->P1.
	//Ni potrebno predhodno pognati FindEdges, ker ta funkcija ne uporablja Xedges, Yedges!
	//minDebelina je minimalna debelina roba v pikslih: če je rob tanjši, ga funkcija ne upošteva in išče naslednjega
	//Če je transitionRequired true, potem najdemo samo prehod iz originalne binarne intenzitete v ciljno. Če je false, potem najdemo prvo točko s ciljno binarno intenziteto
	CPointFloat GetEdgePointOnSegment2D(CLine segment, bool forward, float step, bool blackToWhite, float treshold, int minDebelina, bool transitionRequired);

	//Če izpustimo parametra minDebelina in transitionRequired, ju nastavim na 1, true
	CPointFloat GetEdgePointOnSegment2D(CLine segment, bool forward, float step, bool blackToWhite, float treshold);

	//CCircle GetCircle(CRectRotated rect, int whiteThreshold, int blackThreshold, bool whitePoints); //krog iz kvadrata - bel in crn nivo - isce po intenziteti 	//Izraèuna krog iz težišèa in plošèine belih ali èrnih toèk v pravokotniku
//	CCircle GetCircle(CRectRotated polygon); //set circle from edges
	int FindCommonPointsWhiteFast(int threshold, int minPoints, QRect rect);
	int FindCommonPointsBlackFast(int threshold, int minPoints, QRect rect);
	int FindCommonPointsWhiteFastBoundingBox(int threshold, QRect rect);
	int FindCommonPointsBlackFastBoundingBox(int threshold, QRect rect);
	int FindCommonPointsBlackFastBoundingBoxRedused(int threshold, QRect rect, int step);
	int FilterWhiteShapesBySize(int minWidth, int maxWidth, int minHeight, int maxHeight); //filtrira po velikosti
	int FilterWhiteShapesBySize(int minSize, int maxSize); //filtrira po velikosti vse ki imajo eno stranico manjso kot maxSize in drigo vecjo kot minsize
	int FilterWhiteShapesByArea(int minSize, int maxSize); //filtrira po ploščini
	int FilterWhiteShapesInPolygon(std::vector<CRectRotated> polygons); //filtrira ploskve če se središča nahajajo v poligonu
	int FilterBlackShapesBySize(int minWidth, int maxWidth, int minHeight, int maxHeight); //filtrira po velikosti 
	int FilterBlackShapesBySize(int minSize, int maxSize); //filtrira po velikosti vse ki imajo eno stranico manjso kot maxSize in drigo vecjo kot minsize
	int FilterBlackShapesByArea(int minSize, int maxSize); //filtrira po ploščini
	int FilterBlackShapesInPolygon(std::vector<CRectRotated> polygons); //filtrira ploskve če se središča nahajajo v poligonu

	int  FindFreeWhiteShape();
	int	 FindFreeBlackShape();
	int JoinCommonBlackShapes(double maxDistance); //zdruzi ploskve, ki so si dovolj blizu
	bool AddNewWhitePoint(int x, int y, int shapeNumber);
	bool AddNewBlackPoint(int x, int y, int shapeNumber);
	void CopyWhiteShape(int shapeNew, int shapeOld);
	void CopyBlackShape(int shapeNew, int shapeOld);

	//Ales dodaj funkcije nazaj
	//Opomba Martin: GetDistancesAroundRadius poišèe toèke prehoda intenzitete v radialnih smereh od centralne toèke in v objektu circle nastavi min, max in avgRadius na razdalje do center-prehod 
/*	void GetDistancesAroundRadius(CCircle &circle, int radius, int filterBand, int F, int whiteThreshold, int blackThreshold);
	void GetDistancesAroundRadiusBack(CCircle &circle, int radius, int filterBand, int F, int whiteThreshold, int blackThreshold);
	float GetDistanceAroundRadius(CCircle &circle, int r1, int r2, int degree, int F, int whiteThreshold, int blackThreshold);
	float GetDistanceAroundRadiusBack(CCircle &circle, int r1, int r2, int degree, int F, int whiteThreshold, int blackThreshold);
	float GetDistanceAroundRadius(CCircle &circle, int r1, int r2, int degree, int F, int differenceThreshold);
	float GetDistanceAroundRadiusBack(CCircle &circle, int r1, int r2, int degree, int F, int differenceThreshold);
	float GetDistanceAroundRadiusGreen(CCircle &circle, int r1, int r2, int degree, int F, int whiteThreshold, int blackThreshold);
	float GetDistanceAroundRadiusBackGreen(CCircle &circle, int r1, int r2, int degree, int F, int whiteThreshold, int blackThreshold);
	float GetDistanceAroundRadiusGreen(CCircle &circle, int r1, int r2, int degree, int F, int differenceThreshold);
	float GetDistanceAroundRadiusBackGreen(CCircle &circle, int r1, int r2, int degree, int F, int differenceThreshold);
	void GetAverageIntensity(CCircle &circle, int r1, int r2, int F);
	RGBQUAD GetAverageIntensityRGB(CCircle &circle, CDC* pDC, bool draw, int r1, int r2, int F, int degree);
	int GetAverageIntensity(CDC* pDC, bool draw, CCircle &circle, int r1, int r2, int F, int degree);*/

	//Dodal Martin: GetWhiteAndBlackArea vrne površino belega in èrnega znotraj zasukanega pravokotnika
	bool GetWhiteAndBlackArea(CRectRotated* rectRot, float* whiteArea, float* blackArea, float treshold);

	//poišèe težišèe èrnega in belega podmreži pravokotnika: perioda doloèa gostoto podmreže (pri perioda = 1 pregledamo vse toèke). Hkrati izraèuna število belih in èrnih toèk ter srednjo vrednost belega in èrnega
	//pri izraèunu težišèa pikslov ne utežim z intenziteto
	bool GetWhiteAndBlackGravity(CRectRotated rect, int perioda, int* nWhite, int* nBlack, float* avgWhite, float* avgBlack, CPointFloat* whiteGravity, CPointFloat* blackGravity, float treshold);

	//Dodal Martin: izraèuna toèke prvega roba v pravokotniku v smeri direction: 0: x++,  1: x--, 2: y++, 3: y--
	//prej je treba izraèunati robove s FindEdges
	void GetFirstEdgePointsVector(QRect rect, int direction, vector<CPointFloat>& edgePoints);

	//Dodal Martin: izračuna točke prvega roba v zasukanem pravokotniku v smeri direction: 0: [0]->[1], 1: [1]->[0], 2: [0]->[3], 3: [3]->[0]   (številke v oglatih oklepajih so indeksi rectRot.polygonPoints)
	//Ni potrebno predhodno pognati FindEdges, ker išèemo robove z GetEdgePointOnSegment2D, ki uporablja samo intenzitete pikslov
	//nLines je število linij, na katerih iščemo robove
	void GetFirstEdgePointsVectorRotated(CRectRotated& rectRot, int direction, int nLines, float step, bool blackToWhite, float treshold, vector<CPointFloat>& edgePoints, int minThickness, bool transitionRequired);


	//Dodal Martin: isto kot GetFirstEdgePointsVectorRotated, le da poišče prva dva robova
	//Če je blackToWhite true, potem je prvi rob prehod iz črnega v belo, drugi pa iz belega v črno.
	//minDebelina je minimalna debelina roba v pikslih: če je rob tanjši, ga funkcija ne upošteva in išče naslednjega
	//Če je transitionRequired true, potem najdemo samo prehod iz originalne binarne intenzitete v ciljno. Če je false, potem najdemo prvo točko s ciljno binarno intenziteto
	void GetDoubleEdgePointsVectorRotated(CRectRotated& rectRot, int direction, int nLines, float step, bool blackToWhite, float treshold, vector<CPointFloat> edgePoints[2], int minThickness, bool transitionRequired);


	//Vrne povprečno debelino lika znotraj rectRot v smeri direction
	//za izračun točk uporablja GetDoubleEdgePointsVectorRotated
	float GetAverageThickness(CRectRotated& rectRot, int direction, int nLines, float step, bool blackToWhite, float treshold, int minThickness, bool transitionRequired);



	//Dodal Martin:  izraèuna toèke prvega roba v pravokotniku rect iz centra v radialni smeri navzven oz. navznoter (glede na directionOut)
	//ni potrebno prej izraèunati FindEdges, ker išèemo robove z GetEdgePointOnSegment2D, ki uporablja samo intenzitete pikslov
	//išèe samo na kolobarju med Rmin in Rmax, pri tem izpusti vse toèke izven pravokotnika rect
	//foundEdgesShare nastavi na delež najdenih robov (prehodov intenzitete)
	//Èe je omitAngles nastavljen na true, izpustimo kote, pri katerih funkcija OmitAngle(alfa) vrne false
	//Rstep je korak za iskanje prehoda v radialni smeri v pikslih, fiStep pa v kotni smeri v pikslih (fiStep je dolžina loka med dvema smerema iskanja pri Rmax)
	void GetFirstEdgePointsVectorRadial(QRect rect, bool directionOut, float Rstep, float fiStep, bool blackToWhite, float treshold, CPointFloat center, float Rmin, float Rmax, bool omitAngles, bool OmitAngle(float alpha), vector<float>& angles, vector<CPointFloat>& edgePoints, float* foundEdgesShare);


	//izraèuna povpreèni odklon vektorja toèk od znane krožnice (v pikslih)
	//vektor toèk izraèunaš npr. z GetFirstEdgePointVector ali GetFirstEdgePointVectorRadial
	//krog izraèunaš npr. z FindCircleInRectangle
//	float GetDeviationFromCircle(vector<CPointFloat>& points, CCircle* circle);

	//DrawRadiusAtAngle izriše graf razdalje toèk (points) od centra v odvisnosti od kota (angles: v radianih). GraphDxPerDeg ter graphDyPerRadiusPix doloèita merilo v smeri x in y. 
	//Èe doloèimo negativen graphDyPerRadiusPix, potem se merilo izraèuna 
	bool DrawRadiusAtAngle( vector<CPointFloat>& points, CPointFloat center, CPointFloat graphCenter, int graphDxPerDeg, float Rnominal, float yMagnification);


	//Z 2D interpolacijo izraèuna vrednosti intenzitete na kolobarju z radijem R1 do R2, okoli toèke center. Naredi nR korakov v radialni in nFi korakov v kotni smeri
	//prvi indeks teèe v smeri R, drugi v fi
	//vrednosti shranjuje v openCV buffer v CMemoryBuffer objektu stretchRing
	//smer fi pretvori v smer x, smer R v smer y
	//delKroga je 1 za 360 stopinj: èe je nekoliko veèji od 1, zmanjšamo težave z defekti na robu
	bool GetPolarCVbuffer(cv::Mat& stretchedRing, CPointFloat center, float R1, float R2, float delKroga, int nR, int nFi, float* avgIntensity);

	//naloži ustrezno skrèeno sliko imageToPaste v sliko pravokotnik rect slike this 
	//nastavi floatImage na true, èe imageToPaste hrani float intenzitete oziroma false, èe hrani int intenzitete
	bool PasteImage(cv::Mat& imageToPaste, CRectRotated* rect, bool mirrorX, bool mirrorY, bool floatImage);

	//odstrani manjše motnje iz matrike s povpreèenjem
	//filterDirection naj bo 0 za filtriranje v smeri prvega indeksa in 1 za filtriranje v smeri drugega indeksa
	//circular naj bo true, èe je matrika krožna v smeri filtriranja
	//filterInterval doloèi polovièno širino intervala filtriranja: èe je 0, je filtrirana matrika enaka zaèetni
	static bool FilterMatrix1D(vector<vector<float>>& matrix, int filterDirection, bool circular, int filterInterval);

	//odstrani manjše motnje iz matrike s povpreèenjem
	//circular naj bo true, èe je matrika krožna v prvem/drugem indeksu
	//filterInterval[0/1] doloèi polovièno širino intervala filtriranja za smer prvega in smer drugega indeksa: èe je 0, v tej smeri ne filtriramo
	//možno je tudi filtiranje v obeh smereh hkrati
	static bool FilterMatrix2D(vector<vector<float>>& matrix, bool circularFirstCoord, bool circularSecondCoord, int filterIntervalFirstCoord, int filterIntervalSecondCoord);


	//hitrejša razlièica FilterMatrix1D
	static bool FilterMatrix1Dfast(vector<vector<float>>& matrix, int filterDirection, bool circular, int filterInterval);

	//hitrejša razlièica FilterMatrix2D
	static bool FilterMatrix2Dfast(vector<vector<float>>& matrix, bool circularFirstCoord, bool circularSecondCoord, int filterIntervalFirstCoord, int filterIntervalSecondCoord);

	//FilterImage delujejo podobno kot FilterMatrix, toda delajo s cv bufferjem namesto vektorji
	static bool FilterImage1Dfast(cv::Mat& inputImage, cv::Mat& outputImage, int filterDirection, bool circular, int filterInterval);

	static bool FilterImage2Dfast(cv::Mat& inputImage, cv::Mat& outputImage, bool circularFirstCoord, bool circularSecondCoord, int filterIntervalFirstCoord, int filterIntervalSecondCoord);




	//intentitete slike this nastavi na intenzitete v cv bufferju stretchedRing (z interpolacijo pretvori iz polarnih v karteziène koordinate)
	//zoom < 1 omogoèa manjšanja slike, da pospešimo izris: èe je bila matrika redko vzorèena, s tem ne izgubimo kvalitete slike
	//dela s openCV bufferjem
	bool ImageFromPolarCVbuffer(cv::Mat& stretchedRing, CPointFloat center, float R1, float R2, float delKroga, float zoom);

	//rectangle intensity
	int VerticalIntensity(int filter);
	int HorizontalIntensity(int filter);

	//RotatedIntensity povpreči intenzitete po eni dimenziji zasukanega pravokotnika
	//Če je sumDirection01 true, povpreči v smeri polygonPoints[0]->polygonPoints[1], če je false pa v smeri polygonPoints[0]->polygonPoints[3]
	//sumStep in otherStep določita korak v smeri, v kateri povprečimo, in v drugi smeri
	//če je interpolate true, intenzitete izračunamo z interpolacijo, sicer z zaokroževanjem na cele piksle
	int RotatedIntensity(CRectRotated rectRot, bool sumDirection01, int sumStep, int otherStep, bool interpolate, int filter);


	int RadialIntensity(CPointFloat center, int Rmin, int Rmax, int nFi, int filter);  //povpreèi intenzitete v radialni smeri in nariše graf Iavg[fi]
	int AngularIntensity(CPointFloat center, int Rmin, int Rmax, int nFi, int filter); //povpreèi intenzitete v kotni smeri in nariše graf Iavg[R]




	//zazna tocko vsakic ko pride do prehoda
	int DetectTransitions(int whiteThreshold, int blackThreshold);
	int DetectTransitions();
	int DetectTransitionsBackward(int whiteThreshold, int blackThreshold);
	int DetectTransitionsBackward();
	int DetectTransitionsWhiteToBlack(int whiteThreshold, int blackThreshold);
	int DetectTransitionsBlackToWhite(int whiteThreshold, int blackThreshold);
	int DetectTransitionsWhiteToBlackBackWard(int whiteThreshold, int blackThreshold);
	int DetectTransitionsBlackToWhiteBackWard(int whiteThreshold, int blackThreshold);
	int DetectTransitionsRed(int whiteThreshold, int blackThreshold);
	int DetectTransitionsGreen(int whiteThreshold, int blackThreshold);
	int DetectTransitionsGreenBack(int whiteThreshold, int blackThreshold);
	int DetectTransitionsBlue(int whiteThreshold, int blackThreshold);
	int DetectTransitionsGreenToBlackBackWard(int whiteThreshold, int blackThreshold);
	int DetectTransitionsGreenToBlack(int whiteThreshold, int blackThreshold);
	int DetectTransitions(int difference);
	int DetectTransitionsBackward(int difference);
	void Filter(BYTE* pBuf, int width, int height, int depth, int topFocus, int bottomFocus, int maskWidth, int maskHeight);
	int SetDifference(QRect areaRef, QRect area, CMemoryBuffer &bufferRef, CMemoryBuffer &buffer);
	int SetDifference(QRect areaRef, QRect area, CMemoryBuffer &bufferRef, CMemoryBuffer &buffer, float gain);
	int SetDifference(QRect areaRef, QRect area, CMemoryBuffer &bufferRef, CMemoryBuffer &buffer, float gain, int middleInt);
	int GetDifference(QRect area, QRect areaRef, CMemoryBuffer &comparingBuffer, float gain); //vrne vrednost razlike med piksli med slikama
	int GetDifference(QRect area, QRect areaRef, CMemoryBuffer &comparingBuffer); //vrne vrednost kvadratne razlike med slikama
	int GetDifferenceAbsolute(QRect area, QRect areaRef, CMemoryBuffer &comparingBuffer); //vrne vrednost razlike med piksli med slikama
	int GetDifferenceInterpolate(QRect area, QRect areaRef, CMemoryBuffer &comparingBuffer, int step); //vrne razliko filtrirano s 3x3 filtrom med slikama
	int GetMinDifferencePosition(CRectRotated area, CRectRotated areaRef, CMemoryBuffer &bufferRef, int whiteThreshold, int blackThreshold);

	int GetGravityOfMaximum(vector< float> vec, int start, int stop, float threshold); //Opomba Martin: mislim, da ta funkcija izraèuna srednji indeks belega (vseh toèk z intenziteto nad treshold) v vektorju
	int GetGravityOfMaximum(vector< float> vec, int start, int stop);
	int GetGravityOfMinimum(std::vector< float> vec, int start, int stop);

	void ImageBinaryDilationBlack(CMemoryBuffer &source, QRect rect, int threshold, int kernelSize);
	void ResetBufferOnValue(int value);
	void BinarizeImage(CMemoryBuffer &source, QRect rect, int threshold);
	void BinarizeImage(CMemoryBuffer &source, QRect rect, int threshold, int exeptionThreshold, int intensityValue);
	void MedianFilter(CMemoryBuffer &source, QRect rect, int kernelSize);
	void MedianBinaryFilter(CMemoryBuffer &source, QRect rect, int kernelSize);
	void GetHistogram(CRectRotated rect);
	void GetRelativeFrequencies(Rect region);
	int GetBestMatchValue(CMemoryBuffer &comparingBuffer, CMemoryBuffer &drawBuffer, QRect comparingArea);


	QGraphicsPixmapItem* DrawCommonPointsBoundingBox();
	QGraphicsPixmapItem* DrawCommonPointsBoundingBoxTest(QRect rect);

	QGraphicsPixmapItem * DrawBiggestCommonPoints(QRect rect);

	QGraphicsPathItem* DrawRotatedIntensity(QPen pen);
	QGraphicsPathItem* DrawIntensity(QPen pen);
	QGraphicsPathItem* DrawVerticalIntensity(QPen pen);
	QGraphicsPixmapItem* DrawEdges(QRgb value);


};


