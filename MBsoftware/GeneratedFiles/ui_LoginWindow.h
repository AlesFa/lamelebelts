/********************************************************************************
** Form generated from reading UI file 'LoginWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINWINDOW_H
#define UI_LOGINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoginWindow
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_prijava;
    QPushButton *buttonLogout;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLineEdit *lineEdit_prijava_geslo;
    QComboBox *comboBox_prijava;
    QLabel *label_prijava_upor_ime;
    QLabel *label_prijava_geslo;
    QLabel *label;

    void setupUi(QWidget *LoginWindow)
    {
        if (LoginWindow->objectName().isEmpty())
            LoginWindow->setObjectName(QString::fromUtf8("LoginWindow"));
        LoginWindow->setEnabled(true);
        LoginWindow->resize(464, 191);
        LoginWindow->setAutoFillBackground(true);
        LoginWindow->setStyleSheet(QString::fromUtf8(""));
        horizontalLayoutWidget = new QWidget(LoginWindow);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(130, 130, 321, 51));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton_prijava = new QPushButton(horizontalLayoutWidget);
        pushButton_prijava->setObjectName(QString::fromUtf8("pushButton_prijava"));
        pushButton_prijava->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(pushButton_prijava);

        buttonLogout = new QPushButton(horizontalLayoutWidget);
        buttonLogout->setObjectName(QString::fromUtf8("buttonLogout"));

        horizontalLayout->addWidget(buttonLogout);

        gridLayoutWidget = new QWidget(LoginWindow);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(129, 9, 321, 111));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        lineEdit_prijava_geslo = new QLineEdit(gridLayoutWidget);
        lineEdit_prijava_geslo->setObjectName(QString::fromUtf8("lineEdit_prijava_geslo"));
        lineEdit_prijava_geslo->setEnabled(true);
        lineEdit_prijava_geslo->setMinimumSize(QSize(0, 30));
        lineEdit_prijava_geslo->setMaximumSize(QSize(200, 16777215));
        lineEdit_prijava_geslo->setInputMethodHints(Qt::ImhHiddenText|Qt::ImhNoAutoUppercase|Qt::ImhNoPredictiveText|Qt::ImhSensitiveData);
        lineEdit_prijava_geslo->setInputMask(QString::fromUtf8(""));
        lineEdit_prijava_geslo->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(lineEdit_prijava_geslo, 1, 1, 1, 1);

        comboBox_prijava = new QComboBox(gridLayoutWidget);
        comboBox_prijava->setObjectName(QString::fromUtf8("comboBox_prijava"));
        comboBox_prijava->setMinimumSize(QSize(0, 30));
        comboBox_prijava->setMaximumSize(QSize(200, 16777215));
        comboBox_prijava->setLayoutDirection(Qt::LeftToRight);

        gridLayout->addWidget(comboBox_prijava, 0, 1, 1, 1);

        label_prijava_upor_ime = new QLabel(gridLayoutWidget);
        label_prijava_upor_ime->setObjectName(QString::fromUtf8("label_prijava_upor_ime"));
        label_prijava_upor_ime->setMinimumSize(QSize(0, 40));
        QFont font;
        font.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        font.setPointSize(10);
        label_prijava_upor_ime->setFont(font);

        gridLayout->addWidget(label_prijava_upor_ime, 0, 0, 1, 1);

        label_prijava_geslo = new QLabel(gridLayoutWidget);
        label_prijava_geslo->setObjectName(QString::fromUtf8("label_prijava_geslo"));
        QFont font1;
        font1.setPointSize(10);
        label_prijava_geslo->setFont(font1);

        gridLayout->addWidget(label_prijava_geslo, 1, 0, 1, 1);

        label = new QLabel(LoginWindow);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 22, 101, 111));
        label->setPixmap(QPixmap(QString::fromUtf8("res/LoginBig.bmp")));
        label->setScaledContents(true);

        retranslateUi(LoginWindow);

        QMetaObject::connectSlotsByName(LoginWindow);
    } // setupUi

    void retranslateUi(QWidget *LoginWindow)
    {
        LoginWindow->setWindowTitle(QCoreApplication::translate("LoginWindow", "Login", nullptr));
        pushButton_prijava->setText(QCoreApplication::translate("LoginWindow", "Login", nullptr));
        buttonLogout->setText(QCoreApplication::translate("LoginWindow", "Logout", nullptr));
        lineEdit_prijava_geslo->setText(QString());
        label_prijava_upor_ime->setText(QCoreApplication::translate("LoginWindow", "User:", nullptr));
        label_prijava_geslo->setText(QCoreApplication::translate("LoginWindow", "Password:", nullptr));
        label->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class LoginWindow: public Ui_LoginWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINWINDOW_H
