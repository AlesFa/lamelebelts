#include "stdafx.h"
#include "Measurand.h"

Measurand::Measurand(QObject *parent)
	: QObject(parent)
{

}

Measurand::Measurand()

{

}

Measurand::~Measurand()
{

}

void Measurand::ResetMeasurand(int station,int index)
{
	displayRect[station][index].setRect(0, 0, 0, 0);
	 isActive[station][index] = 0; //trenutno aktivna lamela na sliki
	 isMeasured[station][index] = 0;//je ze izmerjena dimenzijsko
	 isProcessed[station][index] = 0;//meritve so bile sprocesirane
	 sirina[station][index] = 0;
	 visina[station][index] = 0;
	 visinaOdstopanja[station][index] = 0;
	 dolzina[station][index] = 0;
	 angle[station][index] = 0;
	 area[station][index] = 0;
	 odstopanja[station][index] = 0;// primerjava z referencnim kosom in pregled odstopanj
	 speed[station][index] = 0;//hitorst potovanja
	 isMeasuredSpeed[station][index] = 0;//ali ima zmerjeno hjitrost
	 processTime[station][index] = 0; //cas obdelave lamele + hitrost iskanja lamele 
	 nrBlobs[station][index] = 0;//detekcija pack na lameli
	 maxBlobSize[station][index] = 0;//najvecja packa velikost
	 maxBlobCenter[station][index] = CPointFloat(0, 0);//center najvecjega bloba
	 for (int i = 0; i < NR_CENTER; i++)
	 {
		 center[station][index][i] = CPointFloat(0, 0);
		 imageIndexObdelava[station][index][i] = -1;
	 }
	 measureSpeedCounter[station][index] = 0;//stevilo izmerjenih centrov za izracun hitrosti
	 for (int i = 0; i < 360; i++)
	 {
		 distanceFromCenter[station][index][i] = 0;
		 tockeLamele[station][index][i] = CPointFloat(0, 0);
	 }
	 tockeKrogaZgoraj[station][index].clear();
	 tockeKrogaSpodaj[station][index].clear();
	 distanceToCircleZgoraj[station][index].clear();
	 distanceToCircleSpodaj[station][index].clear();
	 isGood[station][index] = 0;
	 centerxx[station][index] = 0; // center y 



}
