#pragma once
class Signals
{
public:
	Signals()throw();
	Signals(QString name, UCHAR address, bool isActive)throw();
	Signals(bool input, QString name, UCHAR address, QString source)throw();
	Signals(QString name, int address, int type, bool isActive);
	Signals(QString name, int address, int type, bool isActive, int bitnumber)throw();
	Signals(const Signals& signal);// Copy constructor
	~Signals();

public:
	QString				name;		//signal name
	int					address;		//digital address
	int					bitNumber;		//strevilka bita pri binarnem primerjanju
	bool				isActive;
	bool				isWritten;		//je bil vpisan na izhod
	int					value;			//signal value
	int					prevValue;		//previous value
	float				valueFloat;			//signal value
	float				prevValueFloat;		//previous value
	bool				valueBool;			//signal value
	bool				prevValueBool;		//previous value
	int					type;		//0 = integer, 1 = float; 2 = boolean
	int					counter;
	int					delay;
	int					isBlinking;	//signal utripa
	int					isInput;  // - signal je input 

									//for trigger
	int timerFrequency; // teoretical timer frequency - for ratio calculation
	int triggingFrequency; // working frequency of selected object (camera, valve, light ...)
	int triggerStartPos;

	int cycle;
	int triggerPosition;	//next trigging position
	int numberOfInterrupts; // timerFrequency/triggingFrequency
	int numberOfInterruptsInCycle; // cycle/triggingNumber
	int triggerDuration = 0;
	int triggerEnable = 0;
	int triggerCounter = 0;

	// object frequency variabless
	int flagInterruptFrequency = 0;
	int interruptCounter = 0;
	int interruptFrequency = 0;

public:
	void SetTrigger(int cycle, int timerFrequency, int triggingFrequency, int triggerStartPos, int triggerDuration, int tableLength, int triggEnable);
	void SetTrigger(int triggerDuration, int tableLength);
	void SetTrigger(int triggerDuration);
	bool GetTriggerPulse(int counter);
	void SetSignal(QString name, int address, int isInput);
	void SetSignal(QString name, int address, bool isActive);
	void SetSignal(QString name, int address, int type, bool isActive);
	void SetInput(QString name, UCHAR address, QString source);
	void SetOutput(QString name, UCHAR address, QString source);
	//void Print(int x, int y, int width, int height, CDC* pDC);
	//void PrintValues(int x, int y, int width, int height, CDC* pDC);
	//int IsEncoderReady(int index, int encoderValue);
	//void ClearEncoderValues(int encoderStart, int encoderEnd);
	//void ClearEncoderValuesBack(int encoderIndex);
};

