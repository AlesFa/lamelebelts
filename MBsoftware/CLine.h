#pragma once
#include "stdafx.h"
#include <qline.h>
#include "CPointFloat.h"







class CLine :public CPointFloat
{
	//DECLARE_DYNAMIC(CLine)
	
public:
	
	CLine()throw();
	CLine(float x1, float y1, float x2, float y2)throw();
	CLine(CPointFloat point1, CPointFloat point2)throw();
	CLine(CPointFloat point, double alfaRadians)throw();
	CLine(CPointFloat point, int alfaRadians)throw();
	CLine(float k, float n)throw();
	CLine(std::vector<CPointFloat>& points)throw();
	CLine(const CLine& line);// Copy constructor

	virtual ~CLine();

public:
	int count;
	int type;
	double k;
	double n;
	CPointFloat p1;
	CPointFloat p2;
	QString data;

protected:
	//DECLARE_MESSAGE_MAP()

public:
	CLine operator=(CLine line);
	bool operator ==(CLine line);
	bool operator !=(CLine line);

	//nastavljanje premice
	void SetLine();											//nastavi premico glede na tocki p1 in p2 - ki sta bili predhodno ze nastavljeni
	void SetLine(CPointFloat point1, CPointFloat point2);	//nastavi premico glede na tocki point1 in point2 - ki sta podani z argumentom. Tocki postaneta p1 in p2
	void SetLine(float x1, float y1, float x2, float y2);	//nastavi premico glede na tocki podani s komponentama. Tocki postaneta p1 in p2
	void SetLine(float k, float n);				//premico nastavi s koeficientom in n, tocki p1 in p2 nastavi na 0 in na 500, x ali y je odvisen od naklona
	void SetLine(CPointFloat point, double alfaRadians);	//premico izracuna s  tocko in kotom: k = tan(alfaRadians); n = -k * point.x + point.y; p1 in p2 sta enaki
	void SetLine(CPointFloat point, int alfaDegrees);
	void SetLine(std::vector<CPointFloat>& points);
	void SetLine(float* x, float* y, int nPoints);  //potegne premico skozi n tock: dela hitreje kot varianta SetLine(vector): isti algoritem, toda uporablja arrays namesto vektorjev


	//funkcije
	void SetAbscissa(double y);											//this premico nastavi kot absciso
	void SetOrdinate(double x);											//this premico nastavi kot ordinato
	CLine GetAbscissa(double y);										//vrne premico, ki je abscisa
	CLine GetOrdinate(double x);										//vrne preimco, ki je ordinata
	CPointFloat GetIntersectionPoint(CLine line);						//vrne tocko, ki je presecisce med this in line premico	
	CLine GetIntersectionLine(CPointFloat point, double alfaRadians);	//vrne premico, ki jseka this premico v tocki point in je pod koto alfaRAdians
	CLine GetIntersectionLine(CPointFloat point, int alfaDegrees);

	//meritve
	double GetDistance(CPointFloat point);								//vrne razdaljo med this in point tocko						
	double GetDistance(CLine line, CPointFloat point);					//vrne razdaljo med tocko, ki je presecisce med this in line premico in tocko point	
	double GetLineAngleInRadians(CLine line);							//vrne kot med this premico in line v radianih
	double GetLineAngleInRadians(void);									//vrne kot med absciso in this premico
	double GetLineAngleInDegrees(CLine line);							//enako kot zgoraj le da v stopinjah
	double GetLineAngleInDegrees(void);									// -II-


	CPointFloat GetPointAtDistance(CPointFloat point, double alfaRadians, double distance);		//vrne tocko, ki je od tocke point oddaljena za distance in je pod kotom alfaRadians 
	CPointFloat GetPointAtDistance(CPointFloat point, int alfaDegrees, double distance);
	CPointFloat GetPointAtDistance(CLine line, double distance);								//vrne tocko, ki je od presecisca med this premico in line oddaljena za distance in lezi na preici line
	CPointFloat GetPointAtDistance(CPointFloat point, double distance);							//vrne tocko, ki je od tocke point oddaljena za distance in lezi na premici this
	CPointFloat GetPointAtDistanceP1(double distance);											//vrne tocko, ki je od tocke p1 oddaljena  za distance in lezi na premici this
	CPointFloat GetPointAtDistanceP2(double distance);											//vrne tocko, ki je od tocke p2 oddaljena  za distance in lezi na premici this

	CLine GetLine();																			//vrne premico ki je nastavljena skozi tocki p1 in p2, ki sta bili predhodno nastavljeni
	CLine GetLine(CPointFloat point1, CPointFloat point2);										//vrne premico ki je nastavljena skozi tocki point1 in point2
	CLine GetLine(float x1, float y1, float x2, float y2);										//vrne premico, ki je nastavljena z k in n	
	CLine GetLine(float k, float n);
	CLine GetLine(CPointFloat point, double alfaRadians);
	CLine GetLine(CPointFloat point, int alfaDegrees);
	CLine GetPerpendicular(CPointFloat p);														//vrne pravokotnico na this premico skozi  tocko p
	CLine GetParallel(CPointFloat p);															//vrne vzporednico na this premico skozi tocko p
	CLine GetParallel(double distance);															//vrne vzporednico na this premico, ki je od nje oddaljena za distance
	void Clear();//vse spremenljivke klasa inicializira na 0

	QGraphicsLineItem* DrawLine(QRect area,QPen pen);
	QGraphicsLineItem * DrawSegment(QPen pen2, int width);
};

