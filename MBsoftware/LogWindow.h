#ifndef LOGWINDOW_H
#define LOGWINDOW_H

#include <QWidget>
#include "ui_LogWindow.h"

class LogWindow : public QWidget
{
	Q_OBJECT

public:
	LogWindow(QWidget *parent = 0);
	~LogWindow();
	void AppendMessage(const QString& text);
	void ShowDialog();
	void SetErrorMessage(int errNr, QString msgContent);  //msgType: 0 = warning; 1 = ereror; 2 = info
	void SetWarningMessage(int errNr, QString msgContent);  //msgType: 0 = warning; 1 = ereror; 2 = info
	void SetInfoMessage(int errNr, QString msgContent);  //msgType: 0 = warning; 1 = ereror; 2 = info
	void ClearWindowMessage();
	void ClearWindowMessage(int index);
	void ClearLastWindowMessage();
	int FindErrorFlagValue(int value);

public:
	std::vector<QString>	errorMessage;
	std::vector<int>		errorFlag;
	std::vector<int>		msgType; //0 = ok; 1 = ereror; 2 = info
	int						maxLogs = 12;
	QReadWriteLock			lockWrite;
private:
	Ui::LogWindow ui;
	QFile *m_logFile;
	QString fileName;
};

#endif // LOGWINDOW_H
