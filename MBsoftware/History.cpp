#include "stdafx.h"
#include "History.h"
/*
History::History(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}
*/
History::History()
{
	ui.setupUi(this);
	Init(); 
}

History::~History()
{
	
		delete parametersTable[0];
	
	if (isSceneAdded == true)
	{
		for (int i = 0; i < 4; i++)
		{
			qDeleteAll(showScene[i]->items());
		}
	}
}

void History::Init()
{
	for (int i = 0; i < NR_STATIONS; i++)
	{
		lastIn[i] = 0;
		displayCurrent[i] = 0;
	}
	isSceneAdded = false;
	currentStation = 0;

	for (int i = 0; i < 50; i++)
	{
		ui.historyList->addItem("");
	}
	for (int i = 0; i < HISTORY_SIZE; i++)
	{
		isScaled[i] = false;
	}
	connect(ui.buttonPlus, SIGNAL(pressed()), this, SLOT(OnButtonPlus()));
	connect(ui.buttonMinus, SIGNAL(pressed()), this, SLOT(OnButtonMinus()));
	connect(ui.buttonUpload, SIGNAL(pressed()), this, SLOT(OnButtonUpload()));


}

void History::OnShowDialog(int station)
{
	int curr;
	currentHis = 0;
	displayCurrent[station] = 0;
	currentStation = station;
	
	curr = lastIn[station] -1;
	if (curr < 0)
		curr = HISTORY_SIZE - 1;
	currentHis = curr;
	setWindowState(Qt::WindowActive);
	DrawMeasurements(curr);
	//DrawImageOnScreen(curr);
	show();
}

void History::UpdateCounter(int station)
{
	lastIn[station]++;
	lastIn[station] = lastIn[station] % HISTORY_SIZE;
}

void History::DrawImageOnScreen(int nr)
{
	if (!isSceneAdded)
	{
		for (int i = 0; i < 4; i++)
		{
			showScene[i] = new QGraphicsScene(this);
		}
		ui.imageViewMainWindow0->setScene(showScene[0]);
		
		isSceneAdded = true;

		ui.imageViewMainWindow0->scale(1, 1);
		

	}
	for (int i = 0; i < 4; i++)
	{
		qDeleteAll(showScene[i]->items());
	}

	//ui.imageViewMainWindow->setGeometry(QRect(0, 0, currImage->buffer->cols, currImage->buffer->rows));
	ui.imageViewMainWindow0->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	

	cv::Mat test;

	
		if (cam0Image[currentStation][nr].channels() == 1)
			cvtColor(cam0Image[currentStation][nr], test, cv::COLOR_GRAY2RGB);
		else
			cvtColor(cam0Image[currentStation][nr], test, cv::COLOR_BGR2RGB);


	
		//DrawImage = test(Rect(0, 0, test.cols, test.rows));

		QImage qimgOriginal((uchar*)test.data, test.cols, test.rows, test.step, QImage::Format_RGB888);

		showScene[0]->addPixmap(QPixmap::fromImage(qimgOriginal));


		ui.labelCounter->setText(QString("HISTORY: %1 / %2 ").arg(displayCurrent[currentStation] +1 ).arg(HISTORY_SIZE));

}

void History::OnButtonPlus()
{
	int curr;
	
	displayCurrent[currentStation]--;
	if (displayCurrent[currentStation] < 0)
		displayCurrent[currentStation] = HISTORY_SIZE -1;


	curr = ((lastIn[currentStation]-1) - displayCurrent[currentStation] + HISTORY_SIZE) % HISTORY_SIZE;
	currentHis = curr;
	DrawMeasurements(curr);
}

void History::OnButtonMinus() //se pomikam nazaj po zgodovini
{
	int curr;
	displayCurrent[currentStation]++;
	if (displayCurrent[currentStation] >= HISTORY_SIZE)
		displayCurrent[currentStation] = 0;
	
	curr = ((lastIn[currentStation]-1) - displayCurrent[currentStation] + HISTORY_SIZE) % HISTORY_SIZE;
	currentHis = curr;
	DrawMeasurements(curr);
}

void History::OnButtonUpload()
{
	emit callFromHistory(currentStation,currentHis);
}


void History::CreateMeasurementsTable()
{
	int height = 0;

		parametersTable[0] = new QTableWidget();


	//measurementsBox
	parametersTable[0]->setStyleSheet("QHeaderView::section { background-color:rgb(236, 236, 236); }");
	parametersTable[0]->setMinimumWidth(420);

	int test = parametersCounter[0][0];

	height = (parametersCounter[0][0]) * 25;
	parametersTable[0]->setMinimumHeight(height + 25);
	parametersTable[0]->setMinimumWidth(420);

	parametersTable[0]->setEditTriggers(QAbstractItemView::NoEditTriggers); //diable edit
	parametersTable[0]->setSelectionMode(QAbstractItemView::NoSelection); //diable edit
	QStringList celice;
	parametersTable[0]->setColumnCount(4);
	celice << "Parameter" << "Measured" << "Min" << "Max";
	parametersTable[0]->setHorizontalHeaderLabels(celice);
	parametersTable[0]->setColumnWidth(0, 160);
	parametersTable[0]->setColumnWidth(1, 85);
	parametersTable[0]->setColumnWidth(2, 85);
	parametersTable[0]->setColumnWidth(3, 80);

	parametersTable[0]->setRowCount(parametersCounter[0][0]);
	parametersTable[0]->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ui.frameMeasurements->setMinimumHeight(height + 100); //25 za naslov nad meritvami
	//ui.frameMeasurements->setMinimumWidth(parametersTable[0]->width());
//	ui.layoutMeasurements->SetMinimumSize;
	ui.layoutMeasurements->addWidget(parametersTable[0], Qt::AlignTop);




}

void History::DrawMeasurements(int currentNr)
{

	/*for (int i = 0; i < NR_STATIONS; i++)
	{
		if (currentType != prevType)
		{
			prevType = currentType;

			if (parametersTable[i]->rowCount() != types[i][currentType]->parameterCounter)
			{
				parametersTable[i]->setRowCount(types[0][currentType]->parameterCounter);
			}
		}

	}*/
	QColor bcolor;

	//v prihodnosti dodaj se za vec postaj
	//ui.labelCurrentType->setText(types[0][currentType]->typeName);
	//types[0][currentType]->parametersChanged = false;
	parametersTable[0]->setRowCount(parametersCounter[currentStation][currentNr]);

	for (int row = 0; row < parametersTable[0]->rowCount(); row++)
	{
		//ce se je dodal nov parameter, dodamo novo vrstico
		{
			for (int column = 0; column < parametersTable[0]->columnCount(); column++)
			{
				QTableWidgetItem *newItem;
				switch (column)
				{
				case 0:
					newItem = new QTableWidgetItem(tr("%1").arg(name[currentStation][currentNr][row]));
					break;
				case 1:
					newItem = new QTableWidgetItem(tr("%1").arg(measuredValue[currentStation][currentNr][row]));
					break;
				case 2:
					newItem = new QTableWidgetItem(tr("%1").arg(toleranceLow[currentStation][currentNr][row]));
					break;

				case 3:
					newItem = new QTableWidgetItem(tr("%1").arg(toleranceHigh[currentStation][currentNr][row]));
					break;
				default:
					break;
				}
				parametersTable[0]->setItem(row, column, newItem);
			}
		}
	}

	//obarva vrstice dobre slabe
	int good = 0;
	for (int row = 0; row < parametersTable[0]->rowCount(); row++)
	{
		good = IsGood(currentStation,currentNr,row);
		if (isActive[row])
		{
			//measurements[i][vars->selectedType]->IsGood(row);

			if (good == 1)
			{
				bcolor = QColor(150, 255, 150);
			}
			else if (good == 0)
				bcolor = QColor(255, 150, 150);
			else
				bcolor = QColor(0, 170, 255);
		}
		else
		{
			bcolor = QColor(236, 236, 236);
		}


		parametersTable[0]->item(row, 0)->setBackground(bcolor);
		parametersTable[0]->item(row, 1)->setBackground(bcolor);
		parametersTable[0]->item(row, 2)->setBackground(bcolor);
		parametersTable[0]->item(row, 3)->setBackground(bcolor);
		//measurementsResoults->item(row, 1)->setText(QString("%1").arg(types[0][vars->selectedType]->measuredValue[row]));
	}

	DrawImageOnScreen(currentNr);
	DrawData(currentNr);

}

void History::DrawData(int currentNr)
{

	QFont sansFont("Calibri [Bold]", 16);
	QString tmpGoodStr;

	if (isGood[currentStation][currentNr] == 2)
	{
		tmpGoodStr = "DARK";
		ui.labelGood->setStyleSheet(QStringLiteral("background-color: rgb(0, 170, 255);"));
	}
	else if (isGood[currentStation][currentNr] == 1)
	{
		tmpGoodStr = "GOOD";
		ui.labelGood->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
	}
	else
	{
		tmpGoodStr = "BAD";
		ui.labelGood->setStyleSheet(QStringLiteral("background-color: rgb(255, 150, 150);"));
	}
	ui.labelGood->setFont(sansFont);
	ui.labelGood->setText(QString("%1").arg(tmpGoodStr));


	int index = 0;
	ui.historyList->item(index)->setText(QString("Piece Num: %2").arg(pieceNum[currentStation][currentNr]));
	index++;

		ui.historyList->item(index)->setText(QString("Speed: %2").arg(speed[currentStation][currentNr]));
		index++;

		ui.historyList->item(index)->setText(QString("Angle: %2").arg(angle[currentStation][currentNr]));
		index++;
	

}

/*int History::IsGood(int station, int currentNr, int nParam)	//check if nominal is set
{
		if ((measuredValue[station][currentNr][nParam] >= toleranceLow[station][currentNr][nParam]) && (measuredValue[station][currentNr][nParam] <= toleranceHigh[station][currentNr][nParam]))
		{
			return 1;
		}
		else
		{
			return 0;

		}

		return 0;
	
}*/




int History::IsGood(int station, int currentNr, int nParam)	//check if nominal is set
{
	if (nominal[station][currentNr][nParam] == 0.0)
	{
		if ((measuredValue[station][currentNr][nParam] >= (toleranceLow[station][currentNr][nParam])) && (measuredValue[station][currentNr][nParam] <= (toleranceHigh[station][currentNr][nParam])))
		{

			return 1;
		}
		else
		{
			if (isConditional[station][currentNr][nParam] == 1)
			{
				if (abs(toleranceHighCond[station][currentNr][nParam] + toleranceLowCond[station][currentNr][nParam]) > 0) //pogojna parametra
				{
					if ((measuredValue[station][currentNr][nParam] >= (toleranceLowCond[station][currentNr][nParam])) && (measuredValue[station][currentNr][nParam] <= (toleranceHighCond[station][currentNr][nParam])))
					{

						return 2;
					}
				}
			}

			return 0;
		}

	}
	else //nominal is set check tolerance + nominal 
	{
		if ((measuredValue[station][currentNr][nParam] >= (toleranceLow[station][currentNr][nParam] + nominal[station][currentNr][nParam])) && (measuredValue[station][currentNr][nParam] <= (toleranceHigh[station][currentNr][nParam] + nominal[station][currentNr][nParam])))
		{
			return 1;
		}
		else
		{
			if (isConditional[station][currentNr][nParam] == 1)
			{
				if (abs(toleranceHighCond[station][currentNr][nParam] + toleranceLowCond[station][currentNr][nParam]) > 0) //pogojna parametra
				{
					if ((measuredValue[station][currentNr][nParam] >= (nominal[station][currentNr][nParam] - toleranceLowCond[station][currentNr][nParam])) && (measuredValue[station][currentNr][nParam] <= (nominal[station][currentNr][nParam] + toleranceHighCond[station][currentNr][nParam])))
					{

						return 2;
					}
				}

			}

			return 0;

		}
	}
	return 0;
}


